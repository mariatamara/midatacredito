(function(){
	/*texto seleccionado en button ppal del Dropdown*/
	function textoDropdown(elemtoActual){
		var textoActual = $(elemtoActual).text();
		$("#selectDropdown").text(textoActual);
		$("#unSpan").text(valorSpan + " ... Nuevo");
		return false;
	}
	/*convierte Dropdow en Collapse*/
	function dropdowntocollapse(){
		var screenwidth = $(window).width();
		console.log(screenwidth);
		if (screenwidth > 768) {
			$(".c-collapse").removeClass("dropdown");
			$(".c-collapse ul").removeClass("dropdown-menu");
			$(".c-collapse ul li button").removeClass("dropdown-item");
		} else {
			$(".c-collapse").addClass("dropdown");
			$(".c-collapse ul").addClass("dropdown-menu");
			$(".c-collapse ul li button").addClass("dropdown-item");
		}
	}
	/*fucionamiento Collapse*/
	function collapse(targetCollapse, elemtoActual) {
		$(".collapse-contenido").css("display","none");
		$("#"+targetCollapse).css("display","block");
		$(".btn-collapse").removeClass("active");
		$(elemtoActual).addClass("active");
	}	
	/*clic en button -> collapse + dropdown*/
	$(".btn-collapse").on("click", function() {
		var targetCollapse = $(this).data("targetcollapse");
		var elemtoActual = $(this);
		collapse(targetCollapse, elemtoActual);
		textoDropdown(elemtoActual);
		return false;
	});
	/*cambia de tamaño*/
	$(window).on("resize", function(){
		dropdowntocollapse();
		return false;
	});
	/*iniciando*/
	$(document).ready( function(){
		dropdowntocollapse();
		return false;
	});
})();