# MiDatacredito

Landing para la cuenta DataCredito.

### Prerrequisitos

* Apache 2.
* PHP 7.0.

### Prerrequisitos de desarrollo

* Nodejs.
* Laravelmix


### Instalación

Descripción de la instalación.

* Obtener el repositorio.
* Abrir la ruta del proyecto en una consola y ejecutar: "npm install", luego de que la instalación de paquetes termine, ejecutar "npm run watch", para compilar estilos js y optimización de imágenes en modo dedarrollador, o "npm run production" para compilar los archivos optimizados para ser cargados en producción. Toda la configuración se encuentra en el archivo "webpack.mix.js".


## Configuraciones adicionales



## Desarrollo

Estos son los frameworks y librerías usados para el desarrollo y funcionamineto del sitio.

## Lista de libreías y frameworks

* [Bootstrap 4.1.1](https://getbootstrap.com/)
* [JQuery 3.3.1](https://jquery.com/)
* [Owl Carousel 2.3.4](https://owlcarousel2.github.io/OwlCarousel2/)# Saber es poder


